/*
	Copyright (C) 2023 Gabriel Martins
  
	This software is provided 'as-is', without any express or implied
	warranty.  In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:
  
	1. The origin of this software must not be misrepresented; you must not
   	claim that you wrote the original software. If you use this software
   	in a product, an acknowledgment in the product documentation would be
   	appreciated but is not required. 
	2. Altered source versions must be plainly marked as such, and must not be
   	misrepresented as being the original software.
	3. This notice may not be removed or altered from any source distribution.
*/

#include "nesgfx.h"
#include <stdio.h>
#include <stdlib.h>

#define NES_SIZE_OF_CHARACTER 16
#define GB_SIZE_OF_CHARACTER 16
#define SNES4BPP_SIZE_OF_CHARACTER 32

static void BIT_ModifyBit(uint8_t* byte, uint8_t bit, uint8_t value);
static int BIT_GetBit(uint8_t byte, uint8_t bit);

/* NES 2BPP format */
static uint8_t NESRom_GetCharacterColorNES(NESRom* rom, size_t character, size_t pixel);
static void NESRom_ModifyCharacterNES(NESRom* rom, size_t character, size_t pixel, uint8_t color);

/* GB 2BPP format */
static uint8_t NESRom_GetCharacterColorGB(NESRom* rom, size_t character, size_t pixel);
static void NESRom_ModifyCharacterGB(NESRom* rom, size_t character, size_t pixel, uint8_t color);

/* SNES 4BPP format */
static uint8_t NESRom_GetCharacterColorSNES4BPP(NESRom* rom, size_t character, size_t pixel);
static void NESRom_ModifyCharacterSNES4BPP(NESRom* rom, size_t character, size_t pixel, uint8_t color);

/* SNES 2BPP is actually the same as GB2BPP */

static void BIT_ModifyBit(uint8_t* byte, uint8_t bit, uint8_t value){
	uint8_t mask = 128 >> bit; mask = ~mask;
	uint8_t bit_value = 128 >> bit;

	*byte = *byte & mask;

	if(value) *byte = *byte | bit_value;
}

static int BIT_GetBit(uint8_t byte, uint8_t bit){
	uint8_t mask = 128 >> bit;
	return (byte & mask) && 1;
}

static uint8_t NESRom_GetCharacterColorNES(NESRom* rom, size_t character, size_t pixel){
	/* each character is a 8x8 stored in 16 bytes 
	 * there are 2 bits per pixel
	 * the first bit is stored in the first 8 bytes
	 * and then the second bit is stored in the second 8 bytes */

	size_t byte_to_get = character * NES_SIZE_OF_CHARACTER + (pixel / 8);

	uint8_t color = BIT_GetBit(rom->rom[byte_to_get], pixel % 8) * 1 +
		BIT_GetBit(rom->rom[byte_to_get + 8], pixel % 8) * 2;

	return color;
}

static void NESRom_ModifyCharacterNES(NESRom* rom, size_t character, size_t pixel, uint8_t color){
	size_t byte_to_modify = character * NES_SIZE_OF_CHARACTER + (pixel / 8);
	BIT_ModifyBit(&rom->rom[byte_to_modify], pixel % 8, color & 1);
	BIT_ModifyBit(&rom->rom[byte_to_modify + 8], pixel % 8, color & 2);
}

static uint8_t NESRom_GetCharacterColorGB(NESRom* rom, size_t character, size_t pixel){
	size_t pixel_row = pixel / 8;
	size_t byte_to_get = character * NES_SIZE_OF_CHARACTER + pixel_row * 2;

	uint8_t color = BIT_GetBit(rom->rom[byte_to_get], pixel % 8) * 1 +
		BIT_GetBit(rom->rom[byte_to_get + 1], pixel % 8) * 2;

	return color;
}

static void NESRom_ModifyCharacterGB(NESRom* rom, size_t character, size_t pixel, uint8_t color){
	size_t pixel_row = pixel / 8;
	
	size_t byte_to_modify = character * NES_SIZE_OF_CHARACTER + pixel_row * 2;
	BIT_ModifyBit(&rom->rom[byte_to_modify], pixel % 8, color & 1);
	BIT_ModifyBit(&rom->rom[byte_to_modify + 1], pixel % 8, color & 2);
}

static uint8_t NESRom_GetCharacterColorSNES4BPP(NESRom* rom, size_t character, size_t pixel){
	size_t pixel_row = pixel / 8;
	size_t byte_to_get = character * SNES4BPP_SIZE_OF_CHARACTER + pixel_row * 2;

	uint8_t color = 
		BIT_GetBit(rom->rom[byte_to_get], pixel % 8) * 1 +
		BIT_GetBit(rom->rom[byte_to_get + 1], pixel % 8) * 2 +
		BIT_GetBit(rom->rom[byte_to_get + 16], pixel % 8) * 4 +
		BIT_GetBit(rom->rom[byte_to_get + 17], pixel % 8) * 8;

	return color;
}

static void NESRom_ModifyCharacterSNES4BPP(NESRom* rom, size_t character, size_t pixel, uint8_t color){
	size_t pixel_row = pixel / 8;
	
	size_t byte_to_modify = character * SNES4BPP_SIZE_OF_CHARACTER + pixel_row * 2;
	BIT_ModifyBit(&rom->rom[byte_to_modify], pixel % 8, color & 1);
	BIT_ModifyBit(&rom->rom[byte_to_modify + 1], pixel % 8, color & 2);
	BIT_ModifyBit(&rom->rom[byte_to_modify + 16], pixel % 8, color & 4);
	BIT_ModifyBit(&rom->rom[byte_to_modify + 17], pixel % 8, color & 8);
}

NESRom* NESRom_LoadRom(const char* filename){
	FILE* file = fopen(filename, "rb");
	if(file == NULL) return NULL;

	NESRom* rom = malloc(sizeof(NESRom));

	/* get the size of the rom in bytes */
	fseek(file, 0, SEEK_END);
	rom->size = ftell(file);
	fseek(file, 0, SEEK_SET);

	rom->rom = malloc(rom->size);
	rom->format = 0;

	fread(rom->rom, rom->size, 1, file);
	fclose(file);

	return rom;
}

void NESRom_SaveRom(NESRom* rom, const char* filename){
	if(rom == NULL) return;
	if(rom->rom == NULL) return;
	if(filename == NULL) return;

	FILE* file = fopen(filename, "wb");

	if(file == NULL) return;

	fwrite(rom->rom, rom->size, 1, file);

	fclose(file);
}

void NESRom_SetFormat(NESRom* rom, uint8_t format){
	rom->format = format;
}

size_t NESRom_GetNumberOfCharacters(NESRom* rom){
	if(rom == NULL) return 0;

	switch(rom->format){
		case ROM_FORMAT_NES2BPP:
			return rom->size / NES_SIZE_OF_CHARACTER;
		break;

		case ROM_FORMAT_SNES2BPP:
		case ROM_FORMAT_GB2BPP:
			return rom->size / GB_SIZE_OF_CHARACTER;
		break;

		case ROM_FORMAT_SNES4BPP:
			return rom->size / SNES4BPP_SIZE_OF_CHARACTER;
		break;
	}

	return 0;
}

uint8_t NESRom_GetCharacterColor(NESRom* rom, size_t character, size_t pixel){
	if(rom == NULL) return 0;

	switch(rom->format){
		case ROM_FORMAT_NES2BPP:
			return NESRom_GetCharacterColorNES(rom, character, pixel);
		break;

		case ROM_FORMAT_SNES2BPP:
		case ROM_FORMAT_GB2BPP:
			return NESRom_GetCharacterColorGB(rom, character, pixel);
		break;

		case ROM_FORMAT_SNES4BPP:
			return NESRom_GetCharacterColorSNES4BPP(rom, character, pixel);
		break;
	}

	return 0;
}

void NESRom_ModifyCharacter(NESRom* rom, size_t character, size_t pixel, uint8_t color){
	if(rom == NULL) return;

	switch(rom->format){
		case ROM_FORMAT_NES2BPP:
			NESRom_ModifyCharacterNES(rom, character, pixel, color);
		break;

		case ROM_FORMAT_SNES2BPP:
		case ROM_FORMAT_GB2BPP:
			NESRom_ModifyCharacterGB(rom, character, pixel, color);
		break;

		case ROM_FORMAT_SNES4BPP:
			NESRom_ModifyCharacterSNES4BPP(rom, character, pixel, color);
		break;
	}
}

void NESRom_Destroy(NESRom* rom){
	if(rom == NULL) return;

	if(rom->rom != NULL) free(rom->rom);

	free(rom);
}
