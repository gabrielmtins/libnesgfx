/*
	Copyright (C) 2023 Gabriel Martins
  
	This software is provided 'as-is', without any express or implied
	warranty.  In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:
  
	1. The origin of this software must not be misrepresented; you must not
   	claim that you wrote the original software. If you use this software
   	in a product, an acknowledgment in the product documentation would be
   	appreciated but is not required. 
	2. Altered source versions must be plainly marked as such, and must not be
   	misrepresented as being the original software.
	3. This notice may not be removed or altered from any source distribution.
*/

#ifndef NESGFX_H
#define NESGFX_H

#include <stdint.h>
#include <stddef.h>

enum ROM_FORMAT{
	ROM_FORMAT_NES2BPP = 0,
	ROM_FORMAT_GB2BPP,
	ROM_FORMAT_SNES2BPP,
	ROM_FORMAT_SNES4BPP
};

typedef struct{
	uint8_t* rom;
	uint8_t format;
	size_t size;
} NESRom;

NESRom* NESRom_LoadRom(const char* filename);

void NESRom_SaveRom(NESRom* rom, const char* filename);

void NESRom_SetFormat(NESRom* rom, uint8_t format);

size_t NESRom_GetNumberOfCharacters(NESRom* rom);

uint8_t NESRom_GetCharacterColor(NESRom* rom, size_t character, size_t pixel);

void NESRom_ModifyCharacter(NESRom* rom, size_t character, size_t pixel, uint8_t color);

void NESRom_Destroy(NESRom* rom);

#endif
