# Documentation

## Structures

### NESRom

```
typedef struct{
	uint8_t* rom;
	uint8_t format;
	size_t size;
} NESRom;
```

The NESRom structure contains the rom data.

## Enumerations

### ROM\_FORMAT

```
enum ROM_FORMAT{
	ROM_FORMAT_NES2BPP = 0,
	ROM_FORMAT_GB2BPP,
	ROM_FORMAT_SNES2BPP,
	ROM_FOMRAT_SNES4BPP
};
```

The formats of rom that nesgfx can interact with it.

## Functions

### NESRom\_LoadRom

`NESRom* NESRom_LoadRom(const char* filename);`

Loads a rom from filename.

### NESRom\_SaveRom

`void NESRom_SaveRom(NESRom* rom, const char* filename);`

Saves the rom to filename.

### NESRom\_SetFormat

`void NESRom_SetFormat(NESRom* rom, uint8_t format);`

Sets a rom for a specified format.  
It can be:

### NESRom\_GetNumberOfCharacters

`size_t NESRom_GetNumberOfCharacters(NESRom* rom);`

Returns the number of 8x8 characters of the rom.

### NESRom\_GetCharacterColor

`uint8_t NESRom_GetCharacterColor(NESRom* rom, size_t character, size_t pixel);`

Returns the color of a pixel of a character.  
The pixel value is between 0 and 64, since a character is 8x8 pixels.

### NESRom\_ModifyCharacter

`void NESRom_ModifyCharacter(NESRom* rom, size_t character, size_t pixel, uint8_t color);`

Modifies the color of a pixel in a character. 
The pixel value is between 0 and 64, since a character is 8x8 pixels.

### NESRom\_Destroy

`void NESRom_Destroy(NESRom* rom);`

Frees the memory used by the rom.
