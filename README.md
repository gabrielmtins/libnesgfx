# libnesgfx

libnesgfx is a library to edit characters of a nes rom in C.  

This library is under zlib license

## Install

```
$ make
$ sudo make install
```
